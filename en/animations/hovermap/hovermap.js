
function Hovermap(resources)
{
	Hovermap.resources = resources;
}
Hovermap.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 600, Phaser.CANVAS, 'Hovermap', { preload: this.preload, create: this.create, update: this.update, render:
		this.render,parent:this });
	},


	preload: function()
	{

        this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 600;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', Hovermap.resources.bckg);
		this.game.load.image('redcirc', Hovermap.resources.redcircle);
		this.game.load.spritesheet('industry', Hovermap.resources.pagebtnsprite, 50, 50);
		this.game.load.spritesheet('icon000', Hovermap.resources.icon000, 110, 110);
		this.game.load.spritesheet('icon001', Hovermap.resources.icon001, 110, 110);
		this.game.load.spritesheet('icon002', Hovermap.resources.icon002, 110, 110);
		this.game.load.spritesheet('icon003', Hovermap.resources.icon003, 110, 110);
		this.game.load.spritesheet('icon004', Hovermap.resources.icon004, 110, 110);
		this.game.load.image('slab', Hovermap.resources.slab);

	},

	create: function(evt)
	{

    //Call the animation build function
    this.parent.buildAnimation();

	},

	cycleData: function(evt){

	},

	up: function(evt)
			{

	}
		,
	over: function(evt)
			{

	}
		,
	out: function(evt)
			{

	}
	,

	buildAnimation: function()
	{

	this.nowBtn = "";

     //Background
     this.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
     this.bckg.anchor.set(0.5);

     this.slab = this.game.add.sprite(this.game.world.centerX, 55, 'slab');
     this.slab.anchor.set(0.5);

     this.countDown = 100;

    //STYLE

    var topBannerText = {font:" 35px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 600, align: "Center",lineSpacing: -10 };

    var paraText = {font:"24px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 700, align: "right",lineSpacing: -10, };



  //  "defaulttitle":"Targeting Options",
          //  "introtext":"Click the icons above to see the various targeting options available for Instagram.",


			//Textfield that displays the name of the lower icon.
		this.pfield_1 = this.game.add.text(this.game.world.centerX, 55, Hovermap.resources.defaulttitle, topBannerText);
		this.pfield_1.anchor.set(0.5);

			//Text field that displays the country name.
		this.pfield_2 = this.game.add.text(50, 400, Hovermap.resources.introtext, paraText);
		//this.pfield_2.anchor.set(0.5);

		//Icons that appear at the bottom of the page
		this.industryIconArray = ['icon000','icon001','icon002','icon003','icon004','icon005','icon006','icon007','icon008','icon009','icon010','icon011','icon012','icon013','icon014','icon015','icon000','icon000','icon000','icon000','icon000','icon000','icon000','icon000','icon000','icon000'];


		this.testy = [ " Argentina" , " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " Czech Republic" , " Denmark " ];

		this.indusArray = [this.retail,this.entertainment,this.automotive,this.ecommerce,this.gaming,this.cpg,this.education,this.energyutil,this.financialservice,this.govtpoli, this.healthcare,this.orgassociations,this.proservices,this.technology,this.telecommunications,this.travel]

		this.butArray = new Array();
		this.countryDotArray = new Array();
		this.localTextArray = Hovermap.resources.iconArray;

		this.xLoop = 0;
		this.yLoop = 0;

		this.xLoopPlus = 0;

		this.xLoopTest = 0;



        for (var i=0;i<5;i++){


			if (this.xLoop < 3){
				this.xLoop += 1;

			}else{

				this.yLoop += 1;
				this.xLoop = 1 ;

			}


            //This LAYS OUT THE BUTTONS ALONG THE BOTTOM
	  		var temp = this.game.add.button( (this.xLoop * 200), 170 + (this.yLoop * 120), this.industryIconArray[i], this.actionOnClick, this, 1, 0, 2);


	  		if (this.yLoop == 1 || this.yLoop == 3 ){

				 temp.x += 100;

			}

			temp.btnId = i;

			temp.scale.x = 1;
			temp.scale.y = 1;

            ///This is the array data coming from the JSON
			temp.textData = this.localTextArray[i];

		    temp.anchor.set(0.5);
			//temp.alpha = 0;

			var speedAlpha = 500 + i*100;
			var tween1 = this.game.add.tween(temp).to({alpha: 1}, 100, Phaser.Easing.Exponential.Out, true, speedAlpha);

			this.butArray.push(temp);

		};


    this.redcirc = this.game.add.sprite(this.game.world.centerX - 1000, this.game.world.centerY-1000, 'redcirc')

    this.redcirc.anchor.set(0.5);

	},

	butOff: function (evt){

	},

	actionOnClick: function(evt){

    this.redcirc.x = evt.x;
	this.redcirc.y = evt.y;

	this.redcirc.scale.x = .60;
	this.redcirc.scale.y = .60;

    this.pfield_2.setText("");

	this.testy2 = new Array();
    this.testy2 = this.indusArray[evt.btnId];

	this.localArray = Hovermap.resources.iconArray;
	this.pfield_1.setText(this.localArray[evt.btnId]);

	this.localTextArray = Hovermap.resources.textArray;
	this.pfield_2.setText(this.localTextArray[evt.btnId]);

	},

	render: function()
	{

	}

}
